import pandas as pd
import json
import influxdb
import websocket
import datetime

currency_pair_ids = {
'7':'BTC_BCN',
'14':'BTC_BTS',
'15':'BTC_BURST',
'20':'BTC_CLAM',
'25':'BTC_DGB',
'27':'BTC_DOGE',
'24':'BTC_DASH',
'38':'BTC_GAME',
'43':'BTC_HUC',
'50':'BTC_LTC',
'51':'BTC_MAID',
'58':'BTC_OMNI',
'61':'BTC_NAV',
'64':'BTC_NMC',
'69':'BTC_NXT',
'75':'BTC_PPC',
'89':'BTC_STR',
'92':'BTC_SYS',
'97':'BTC_VIA',
'100':'BTC_VTC',
'108':'BTC_XCP',
'114':'BTC_XMR',
'116':'BTC_XPM',
'117':'BTC_XRP',
'112':'BTC_XEM',
'148':'BTC_ETH',
'150':'BTC_SC',
'153':'BTC_EXP',
'155':'BTC_FCT',
'160':'BTC_AMP',
'162':'BTC_DCR',
'163':'BTC_LSK',
'167':'BTC_LBC',
'168':'BTC_STEEM',
'170':'BTC_SBD',
'171':'BTC_ETC',
'174':'BTC_REP',
'177':'BTC_ARDR',
'178':'BTC_ZEC',
'182':'BTC_STRAT',
'184':'BTC_PASC',
'185':'BTC_GNT',
'187':'BTC_GNO',
'189':'BTC_BCH',
'192':'BTC_ZRX',
'194':'BTC_CVC',
'196':'BTC_OMG',
'198':'BTC_GAS',
'200':'BTC_STORJ',
'201':'BTC_EOS',
'204':'BTC_SNT',
'207':'BTC_KNC',
'210':'BTC_BAT',
'213':'BTC_LOOM',
'221':'BTC_QTUM',
'121':'USDT_BTC',
'216':'USDT_DOGE',
'122':'USDT_DASH',
'123':'USDT_LTC',
'124':'USDT_NXT',
'125':'USDT_STR',
'126':'USDT_XMR',
'127':'USDT_XRP',
'149':'USDT_ETH',
'219':'USDT_SC',
'218':'USDT_LSK',
'173':'USDT_ETC',
'175':'USDT_REP',
'180':'USDT_ZEC',
'217':'USDT_GNT',
'191':'USDT_BCH',
'220':'USDT_ZRX',
'203':'USDT_EOS',
'206':'USDT_SNT',
'209':'USDT_KNC',
'212':'USDT_BAT',
'215':'USDT_LOOM',
'223':'USDT_QTUM',
'129':'XMR_BCN',
'132':'XMR_DASH',
'137':'XMR_LTC',
'138':'XMR_MAID',
'140':'XMR_NXT',
'181':'XMR_ZEC',
'166':'ETH_LSK',
'169':'ETH_STEEM',
'172':'ETH_ETC',
'176':'ETH_REP',
'179':'ETH_ZEC',
'186':'ETH_GNT',
'188':'ETH_GNO',
'190':'ETH_BCH',
'193':'ETH_ZRX',
'195':'ETH_CVC',
'197':'ETH_OMG',
'199':'ETH_GAS',
'202':'ETH_EOS',
'205':'ETH_SNT',
'208':'ETH_KNC',
'211':'ETH_BAT',
'214':'ETH_LOOM',
'222':'ETH_QTUM',
'224':'USDC_BTC',
'226':'USDC_USDT',
'225':'USDC_ETH',
}

ws = websocket.create_connection("wss://api2.poloniex.com:443")

ticker = 1002
ws.send('{"command" : "subscribe", "channel" : %i}'%ticker)

columns=['currency_pair','last_trade_price','lowest_ask','highest_bid','percent_change_in_last_24_hours','base_currency_volume_in_last_24_hours','quote_currency_volume_in_last_24_hours','is_frozen','highest_trade_price_in_last_24_hours','lowest_trade_price_in_last_24_hours']

client = influxdb.DataFrameClient('influxdb', '8086', 'poloniex', 'poloniex', 'poloniex')

keepgoing = True
while keepgoing:
    if ws.connected:
        frame = json.loads(ws.recv())
        if len(frame) >= 3:
            data=frame[2]
            try:
                data[0] = currency_pair_ids[str(data[0])]
            except(KeyError):
                data[0] = str(data[0])
                print('unknown ID: %s'%str(data[0]))
            
            df = pd.DataFrame(data=[data],columns=columns)
            df.last_trade_price = pd.to_numeric(df.last_trade_price)
            df.lowest_ask = pd.to_numeric(df.lowest_ask)
            df.highest_bid = pd.to_numeric(df.highest_bid)
            df.percent_change_in_last_24_hours = pd.to_numeric(df.percent_change_in_last_24_hours)
            df.base_currency_volume_in_last_24_hours = pd.to_numeric(df.base_currency_volume_in_last_24_hours)
            df.quote_currency_volume_in_last_24_hours = pd.to_numeric(df.quote_currency_volume_in_last_24_hours)
            df.highest_trade_price_in_last_24_hours = pd.to_numeric(df.highest_trade_price_in_last_24_hours)
            df.lowest_trade_price_in_last_24_hours = pd.to_numeric(df.lowest_trade_price_in_last_24_hours)
            df.is_frozen = df.is_frozen==1
            d = datetime.datetime.now()
            df['datetime'] = pd.to_datetime(str(d))
            df['hour'] = pd.to_numeric(d.hour)
            df['day'] = pd.to_numeric(d.day)
            df['month'] = pd.to_numeric(d.month)
            df['year'] = pd.to_numeric(d.year)
            df['weekday'] = pd.to_numeric(d.weekday())
            
            df = df.set_index('datetime')
            client.write_points(df, 'ticks', tag_columns=['currency_pair','is_frozen'], protocol='json', database='poloniex')
